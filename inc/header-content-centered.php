<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!-- FAVICON  -->
  <!-- FAVICON END -->
  <!-- PAGE DETAILS -->
  <?php $websiteName = "Template";?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?> | <?php echo $websiteName;?> </title>
  <!-- CSS STYLE SHEETS -->
  <link rel="stylesheet" href="/css/styles.css">
  <link rel="stylesheet" href="/css/animate.css">
  <!-- PAGE DETAILS END -->
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,600,700" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->

  <!-- COOKIE POLICY -->
</head>
<body>
  <div class="overlay-nd-3"></div>
  <header class="site-header-3" id="myHeader">
    <div class="container-3 site-header-3">
      <div class="logo-header-3">
        <a class="logo-3" href="/index.php">
          <img class="logo-image-3" src="https://equipping4eministry.files.wordpress.com/2013/04/mailchimp-logo-750.png" alt="">
        </a>
      </div>
      <ul class="menu-list-3">
        <li><a href="/">Home</a></li>
        <li><a href="https://www.grabient.com/">Grabient - Color picker</a></li>
        <li><a href="https://realfavicongenerator.net/">Fav Icon</a></li>
        <li><a href="/views/different-menu.php">Different Menu</a></li>
        <li><a href="/views/different-menu-2.php">Header Content Center</a></li>
      </ul>
      <div id="menu-button-3" role="button-3" class="menu-button-container-3" title="Hamburger Menu open/close-3" style="display: none;">
        <div class="hamburger-3" style="display: none;">
          <div class="inner-3"></div>
        </div>
    </div>
  </header>