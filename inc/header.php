<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="Transfers - Private Transport and Car Hire HTML Template" />
	<meta name="description" content="Transfers - Private Transport and Car Hire HTML Template">
	<meta name="author" content="themeenergy.com">
  <!-- FAVICON  -->
  <!-- FAVICON END -->
  <!-- PAGE DETAILS -->
  <?php $websiteName = "Template";?>
  <title><?php echo $title; ?> | <?php echo $websiteName;?> </title>
  <!-- CSS STYLE SHEETS -->
  <link rel="stylesheet" href="/css/styles.css">
  <link rel="stylesheet" href="/css/animate.css">
  <!-- PAGE DETAILS END -->
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,600,700" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->

  <!-- COOKIE POLICY -->

  <!-- SCRIPTS -->
  <script src ="/js/instafeed.js" type="text/javascript"></script>
    <script type ="text/javascript">
    var feed = new Instafeed ({
      get: "user",
      userId: "144531919",
      clientId: "8ab8896419c341c4a84290efce0a68bc",
      accessToken: "144531919.1677ed0.c34b6949c7934ff2a4f25b78b211e5d5",
      sortBy: "most-recent",
      limit: 12,
      resolution: "standard_resolution",
      template: "<div class='Grid-item' style='background-image: url({{image}});'><a href='{{link}}' target='_blank'><div class='hover-container'></div></a></div>",
    });
    feed.run();
  </script>
</head>
<body>
  <div class="overlay-nd"></div>
  <header class="site-header sticky" id="myHeader">
    <div class="container site-header">
      <div class="logo-header">
        <a class="logo" href="/index.php">
          <img class="logo-image" src="https://equipping4eministry.files.wordpress.com/2013/04/mailchimp-logo-750.png" alt="">
        </a>
      </div>
      <ul class="menu-list" id="menu-list-shadow">
        <li><a href="/">Home</a></li>
        <li><a href="https://www.grabient.com/">Grabient - Color picker</a></li>
        <li><a href="https://realfavicongenerator.net/">Fav Icon</a></li>
        <li><a href="views/different-menu.php">Different Menu</a></li>
        <li><a href="views/different-menu-2.php">Header Content Center</a></li>
        <li><a href="/views/fonts.php">Fonts</a></li>
      </ul>
      <div id="menu-button" role="button" class="menu-button-container" title="Hamburger Menu open/close">
      <div class="hamburger" style="display: none;">
        <div class="inner"></div>
      </div>
    </div>
  </header>