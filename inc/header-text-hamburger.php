<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!-- FAVICON  -->
  <!-- FAVICON END -->
  <!-- PAGE DETAILS -->
  <?php $websiteName = "Template";?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $title; ?> | <?php echo $websiteName;?> </title>
  <!-- CSS STYLE SHEETS -->
  <link rel="stylesheet" href="/css/styles.css">
  <link rel="stylesheet" href="/css/animate.css">
  <!-- PAGE DETAILS END -->
  <!-- GOOGLE FONT -->
  <link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,600,700" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->

  <!-- COOKIE POLICY -->
</head>
<body>
  <div class="overlay-nd-2"></div>
  <header class="site-header-2 sticky-2" id="myHeader">
    <div class="container-2 site-header-2">
      <div class="logo-header-2">
        <a class="logo-2" href="/index.php">
          <img class="logo-image-2" src="https://equipping4eministry.files.wordpress.com/2013/04/mailchimp-logo-750.png" alt="">
        </a>
      </div>
      <ul class="menu-list-2">
        <li><a href="/">Home</a></li>
        <li><a href="https://www.grabient.com/">Grabient - Color picker</a></li>
        <li><a href="https://realfavicongenerator.net/">Fav Icon</a></li>
        <li><a href="/views/different-menu.php">Different Menu</a></li>
        <li><a href="/views/different-menu-2.php">Header Content Center</a></li>
      </ul>
      <div id="menu-button-2" role="button-2" class="menu-button-container-2" title="Hamburger Menu open/close-2" style="display: none;">
        <div class="hamburger-2" style="display: none;">
          <div class="inner-2"></div>
        </div>
    </div>
  </header>