var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    sass = require('gulp-ruby-sass'),
    imagemin = require('gulp-imagemin'),
    connect = require('gulp-connect-php');
 

gulp.task('styles', function() {
  return sass('scss/styles.scss', { style: 'expanded' })
    .pipe(autoprefixer())
    .pipe(cssnano())
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('connect-sync', function() {
  connect.server({}, function (){
    browserSync({
      // Ip of server
      proxy: '127.0.0.1:8000'
    });
  });

  
  gulp.watch('*.php').on('change', function () {
    browserSync.reload();
  });
  gulp.watch("*.html").on('change', browserSync.reload);
  gulp.watch("*.js").on('change', browserSync.reload);
  gulp.watch("js/*.js").on('change', browserSync.reload);
  gulp.watch("**/**/*.php").on('change', browserSync.reload);
});

gulp.task('watch', ['connect-sync'], function () {
  // gulp.task('watch', function () {
      gulp.watch("scss/**", ['styles']);
      gulp.watch("*.html").on('change', browserSync.reload);
      gulp.watch("*.js").on('change', browserSync.reload);
      gulp.watch("js/*.js").on('change', browserSync.reload);
      gulp.watch("*.php").on('change', browserSync.reload);
});

gulp.task('imageMin', () =>
    gulp.src('src-imgs/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('img/**/*'))
);

gulp.task('default', ['styles', 'connect-sync'], function () {  
  gulp.start('watch');
});