<?php
$title = "Fonts";
include '../inc/header.php'; ?>
<link href="https://fonts.googleapis.com/css?family=Alegreya|Lato:300|Lustria|Montserrat:700|Muli:200|Ovo|Poppins:700|Source+Sans+Pro:300|Titillium+Web:200,300,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cabin|Nunito|Raleway:600|Ubuntu:500" rel="stylesheet">
<div class="page-title">
  <h1>Font Pairs that work well</h1>
</div>
<div class="different-font-style-container-1">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-2">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-3">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-4">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-5">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-6">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-7">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="different-font-style-container-8">
  <h1>Hello World</h1>
  <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse.</p>
</div>
<div class="color-palette-header">
  <h3>Colour Palettes</h3>
</div>
<div class="color-palette-wrapper">
  <div class="box-master color-palette-box-1">
    <h3 class="color-text-one">Check out these colors</h3>
    <p class="color-text-output-a"></p>
    <p class="color-text-output-b"></p>
    <!-- <p class="result"></p> -->
  </div>
  <div class="box-master color-palette-box-2">
    <h3>Check out these colors</h3>
  </div>
  <div class="box-master color-palette-box-3">
    <h3>Check out these colors</h3>
  </div>
    <div class="box-master color-palette-box-4">
    <h3>Check out these colors</h3>
  </div>
  <div class="box-master color-palette-box-5">
    <h3>Check out these colors</h3>
  </div>
  <div class="box-master color-palette-box-6">
    <h3>Check out these colors</h3>
  </div>
    <div class="box-master color-palette-box-7">
    <h3>Check out these colors</h3>
  </div>
  <div class="box-master color-palette-box-8">
    <h3>Check out these colors</h3>
  </div>
  <div class="box-master color-palette-box-9">
    <h3>Check out these colors</h3>
  </div>
</div>
<?php include '../inc/footer.php'; ?>
<script>
  showColorText();
</script>