var numSquares = 6;
var colors = generateRandomColors(numSquares);
var squares = document.querySelectorAll('.square');
var pickedColor = pickColor();
var colorDisplay = document.querySelector('#colorDisplay');

const h1 = document.querySelector('h1');
var messageBox = document.querySelector('#message');
var resetButton = document.querySelector('#reset');

var modeButtons = document.querySelectorAll('.mode');

init();

function init() {
	setupModeButtons();
	setupSquares();
	reset();
}

function setupModeButtons() {
	for (var i = 0; i < modeButtons.length; i++) {
		modeButtons[i].addEventListener('click', function () {
			modeButtons[0].classList.remove("selected");
			modeButtons[1].classList.remove("selected");
			this.classList.add('selected');

			this.textContent === "Easy" ? numSquares = 3 : numSquares = 6; //if else 
			reset();
		});
	}
}

function setupSquares() {
	for (var i = 0; i < squares.length; i++) {
		//add initial colours
		squares[i].style.backgroundColor = colors[i];
		// add click listeners
		squares[i].addEventListener('click', function () {
			// grab color of clicked square
			var clickedColor = this.style.backgroundColor;
			//compare color
			if (clickedColor === pickedColor) {
				h1.style.background = pickedColor;
				messageBox.textContent = 'Correctamundo';
				changeColors(clickedColor);
				resetButton.textContent = 'Play Again?'
			} else {
				this.style.opacity = '0';
				messageBox.textContent = 'Again ya';
				messageBox.style.opacity = '1';
				setTimeout(x, 4000);
				function x() {
					messageBox.style.opacity = '0';
				}
			}
		});
	}
}


function reset() {
	//generate new colors
	colors = generateRandomColors(numSquares)
	//pick new random color from array
	pickedColor = pickColor();
	//change color display
	colorDisplay.textContent = pickedColor;
	resetButton.textContent = "New Colors";
	//change colors of squares
	for (var i = 0; i < squares.length; i++) {
		if(colors[i]) {
			squares[i].style.display = 'block';
			squares[i].style.backgroundColor = colors[i];
			squares[i].style.opacity = '1';
		} else {
			squares[i].style.display = 'none';
		}
		h1.style.background = 'steelblue';
		messageBox.style.opacity = '0';
	}
}

resetButton.addEventListener('click', function(){
	reset();
});

colorDisplay.textContent = pickedColor;


function changeColors(color) {
	//loop through squares
	for (var i = 0; i < squares.length; i++) {
		//change each colors
		squares[i].style.backgroundColor = color;
	}
}

function pickColor() {
	var random = Math.floor((Math.random() * colors.length));
	return colors[random];
}

function generateRandomColors(num) {
	//make array
	var arr = []

	//repeat num times
	for(var i = 0; i < num; i++){
		//getrandom color and psh into array 
		arr.push(randomColor())
	}
	//return array
	return arr;
}

function randomColor() {
	//pick a red from 0 - 255
	var r = Math.floor(Math.random() * 256);
	//pick a green from 0 - 255
	var g = Math.floor(Math.random() * 256);
	//pick a blue from 0 - 255
	var b = Math.floor(Math.random() * 256);
	return "rgb(" + r + ", " + g + ", " + b + ")";
}





